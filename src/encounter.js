"use strict";

var xpLookup = [
	[0, 0, 0, 0],
	[25, 50, 75, 100],
	[50, 100, 150, 200],
	[75, 150, 225, 400],
	[125, 250, 375, 500],
	[250, 500, 750, 1100],
	[300, 600, 900, 1400],
	[350, 750, 1100, 1700],
	[450, 900, 1400, 2100],
	[550, 1100, 1600, 2400],
	[600, 1200, 1900, 2800],
	[800, 1600, 2400, 3600],
	[1000, 2000, 3000, 4500],
	[1100, 2200, 3400, 5100],
	[1250, 2500, 3800, 5700],
	[1400, 2800, 4300, 6400],
	[1600, 3200, 4800, 7200],
	[2000, 3900, 5900, 8800],
	[2100, 4200, 6300, 9500],
	[2400, 4900, 7300, 10900],
	[2800, 5700, 8500, 12700]
];

var crLookup = {
	"0": 10,
	"1/8": 25,
	"1/4": 50,
	"1/2": 100,
	"1": 200,
	"2": 450,
	"3": 700,
	"4": 1100,
	"5": 1800,
	"6": 2300,
	"7": 2900,
	"8": 3900,
	"9": 5000,
	"10": 5900,
	"11": 7200,
	"12": 8400,
	"13": 10000,
	"14": 11500,
	"15": 13000,
	"16": 15000,
	"17": 18000,
	"18": 20000,
	"19": 22000,
	"20": 25000,
	"21": 33000,
	"22": 41000,
	"23": 50000,
	"24": 62000,
	"25": 75000,
	"26": 90000,
	"27": 105000,
	"28": 120000,
	"29": 135000,
	"30": 155000
};

var multiplierLookup = [
	0.5,
	1,
	1.5,
	2,
	2.5,
	3,
	4,
	5
];

var statusColors = {
	trivial: "#28b7e0",
	easy: "#33c62b",
	medium:"#e9aa28",
	hard:"#da1717",
	deadly: "#6b0000"
};

function groupMod(partySize, totalMonsters) {
	var multiplier = 0;
	if (totalMonsters > 0) {
		multiplier++;
	}
	if (totalMonsters > 1) {
		multiplier++;
	}
	if (totalMonsters > 2) {
		multiplier++;
	}
	if (totalMonsters > 6) {
		multiplier++;
	}
	if (totalMonsters > 10) {
		multiplier++;
	}
	if (totalMonsters > 14) {
		multiplier++;
	}

	if (partySize < 1) {
		multiplier++;
	}
	else if (partySize >= 1 && partySize < 3) {
		multiplier++;
	}
	else if (partySize >= 6) {
		multiplier--;
	}

	return multiplierLookup[multiplier];
}

module.exports = {
	xpLookup,
	crLookup,
	multiplierLookup,
	statusColors,
	party: function (players, allies) {
		var party = new Array();
		for (var p in players) {
			for (i = 0; i < players[p].number; i++) {
				party.push(players[p].level);
			}
		}
		for (var a in allies) {
			var xp = crLookup[allies[a].cr] * 1.5;
			var level = 0;
			while (level < 20 && xpLookup[level][2] < xp) {
				level++;
			}
			for (var i = 0; i < allies[a].number; i++) {
				party.push(level);
			}
		}
		return party;
	},
	thresholds: function (party) {
		var xp = xpLookup;
		var thresholds = [0, 0, 0, 0];
		for (var char in party) {
			for (var i = 0; i < 4; i++) {
				thresholds[i] += xp[party[char]][i];
			}
		}
		return thresholds;
	},
	steps: function (party) {
		var thresholds = this.thresholds(party);
		var midEasy = (thresholds[1] - thresholds[0]) / 2 + thresholds[0];
		var midMedium = (thresholds[2] - thresholds[1]) / 2 + thresholds[1];
		var lowHard = (thresholds[3] - thresholds[2]) / 3 + thresholds[2];
		var midHard = 2 * (thresholds[3] - thresholds[2]) / 3 + thresholds[2];

		return [thresholds[0], Number.parseInt(midEasy), thresholds[1], Number.parseInt(midMedium), thresholds[2], Number.parseInt(lowHard), Number.parseInt(midHard), thresholds[3]];
	},
	encounterXP: function (party, monsters) {
		var totalMonsters = 0;
		var encounterTotal = 0;
		for (var m in monsters) {
			var cr = monsters[m].cr;
			totalMonsters += monsters[m].number;
			var xp = monsters[m].number * crLookup[cr];
			encounterTotal += xp;
		}
		var mod = groupMod(party.length, totalMonsters);
		return mod * encounterTotal;
	},
	getStatusColor: function (num, thresholds) {
		var color;
		//Deadly
		if (num >= thresholds[3]) {
			color = this.statusColors.deadly;
		}
		//Hard
		else if (num >= thresholds[2]) {
			color = this.statusColors.hard;
		}
		//Medium
		else if (num >= thresholds[1]) {
			color = this.statusColors.medium;
		}
		//Easy
		else if (num > thresholds[0]) {
			color = this.statusColors.easy;
		}
		//Trivial
		else {
			color = this.statusColors.trivial;
		}
		return color;
	}
};